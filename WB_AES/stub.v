module stub(
	input [31:0] in,
        input clk, reset,
	output reg [31:0]out );
//reg [31:0] out ;

always @(posedge clk ) begin

	if(!reset) begin
		
		assign out = in;
	end 
end

  
 // integer f;        
        
//initial begin
//$dumpfile("simple.vcd");
//$dumpvars(0,out);
/*  f = $fopen("output.txt");
  $fmonitor(f, "%h", out);
  #1000 
  $fclose(f);
  $finish; */
//end    
    
endmodule

