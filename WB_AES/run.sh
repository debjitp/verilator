#!/bin/bash

verilator --trace --trace-depth 5 --cc --default-language 1364-2001 wb_top.v wb_interconnect_arb_rr.v wb_bus.v aes_top.v aes_192.v table.v round.v stub.v --top-module wb_top --exe tb_top.cpp
make -j -C obj_dir -f Vwb_top.mk Vwb_top
./obj_dir/Vwb_top
