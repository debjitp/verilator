`timescale 1ns/1ps

module wb_top	(
    clk,
     rst,
  ////////////  
busms_dat_i,
busms_err_i,
busms_rty_i,
busms_ack_i,
cipher,

/////////////
   /*  bussl_ack_o,
     bussl_dat_o,
    bussl_err_o,
    //cipher,*/
   ///////////////////////////////
   
busms_adr_o, busms_dat_o,busms_cyc_o,busms_stb_o,busms_sel_o,busms_we_o,busms_cti_o,busms_bte_o
);

    input clk;
    input rst;
    //input irq,
	//output trap,
	//output trace_valid,
	//output [35:0] trace_data,

    //input busms_ack_i,
   // input [31:0] busms_dat_i,
    
   // output bussl_ack_o;
  //output [31:0] bussl_dat_o;
    //output bussl_err_o;
    //output [31:0] cipher;
    //output bussl_rty_o;
   ///////////////////////////////
output busms_rty_i;
output [31:0] busms_dat_i;
output busms_ack_i;
output busms_err_i;
output [31:0] cipher; 

 
input[31:0] busms_adr_o;
input[31:0] busms_dat_o;
input busms_cyc_o;
input busms_stb_o;
input [3:0]busms_sel_o;
input busms_we_o;
input [2:0]    busms_cti_o;
input [1:0]    busms_bte_o;

//import wb_soc_config::*;
//import wb_soc_functions::*;
//`include "./wb_soc_config.sv"
//`include "./wb_soc_functions.sv"

localparam NR_MASTERS = 1;
localparam NR_SLAVES = 1;
localparam SLAVE_MEM   = 0;


   //wire [31:0]   busms_adr_o;
   //wire          busms_cyc_o;
  // wire [31:0]   busms_dat_o;
   //wire [3:0]    busms_sel_o;
 //  wire          busms_stb_o;
  // wire          busms_we_o;
   wire          busms_cab_o;
  // wire [2:0]    busms_cti_o;
   //wire [1:0]    busms_bte_o;
  // wire          busms_ack_i;
  // wire          busms_rty_i;
  // wire          busms_err_i;
  // wire [31:0]   busms_dat_i;

   wire [31:0]   bussl_adr_i;
   wire          bussl_cyc_i;
   wire [31:0]   bussl_dat_i;
   wire [3:0]    bussl_sel_i;
   wire          bussl_stb_i;
   wire          bussl_we_i;
   wire          bussl_cab_i;
   wire [2:0]    bussl_cti_i;
   wire [1:0]    bussl_bte_i;
   wire          bussl_ack_o;
   wire          bussl_rty_o;
   wire          bussl_err_o;
   wire [31:0]   bussl_dat_o;

   wire          snoop_enable;
   wire [31:0]   snoop_adr;




/*	picorv32_wb #(
`ifndef SYNTH_TEST
`ifdef SP_TEST
		.ENABLE_REGS_DUALPORT(0),
`endif
`ifdef COMPRESSED_ISA
		.COMPRESSED_ISA(1),
`endif
		.ENABLE_MUL(1),
		.ENABLE_DIV(1),
		.ENABLE_IRQ(1),
		.ENABLE_TRACE(1)
`endif
	) 
	uut (
        .wb_clk_i(clk),
        .wb_rst_i(rst),

        .trap (trap),
        .irq (irq),
        .trace_valid (trace_valid),
        .trace_data (trace_data),
        .wbm_ack_i(busms_ack_i),
        .wbm_dat_i(busms_dat_i),
        .wbm_adr_o(busms_adr_o),
        .wbm_cyc_o(busms_cyc_o),
        .wbm_dat_o(busms_dat_o),
        .wbm_sel_o(busms_sel_o),
        .wbm_stb_o(busms_stb_o),
        .wbm_we_o(busms_we_o)
);*/


	  aes_top  aes_top_inst 
	
	( // Wishbone interface
            .wb_clk_i(clk),
            .wb_rst_i(rst),
            
            .wb_adr_i(bussl_adr_i),
            .wb_cyc_i(bussl_cyc_i),
            .wb_dat_i(bussl_dat_i),
            .wb_sel_i(bussl_sel_i),
            .wb_stb_i(bussl_stb_i),
            .wb_we_i(bussl_we_i),
            
            .wb_ack_o(bussl_ack_o),
            .wb_dat_o(bussl_dat_o),
            .wb_err_o (bussl_err_o)
     );
     
wb_bus u_bus(
// Outputs
.m_dat_o(busms_dat_i),
.m_ack_o(busms_ack_i),
.m_err_o(busms_err_i),
.m_rty_o(busms_rty_i),
.s_adr_o(bussl_adr_i),
.s_dat_o(bussl_dat_i),
.s_cyc_o(bussl_cyc_i),
.s_stb_o(bussl_stb_i),
.s_sel_o(bussl_sel_i),
.s_we_o(bussl_we_i),
.s_cti_o(bussl_cti_i),
.s_bte_o(bussl_bte_i),
.snoop_adr_o(),
.snoop_en_o(),
.bus_hold_ack(),
// Inputs
.clk_i(clk),
.rst_i(rst),
.m_adr_i(busms_adr_o),
.m_dat_i(busms_dat_o),
.m_cyc_i(busms_cyc_o),
.m_stb_i(busms_stb_o),
.m_sel_i(busms_sel_o),
.m_we_i(busms_we_o),
.m_cti_i(busms_cti_o),
.m_bte_i(busms_bte_o),
.s_dat_i(bussl_dat_o),
.s_ack_i(bussl_ack_o),
.s_err_i(bussl_err_o),
.s_rty_i(bussl_rty_o),
.bus_hold()
);

stub st (
        .clk(clk),
        .reset(rst),
        .in(bussl_dat_o),
        .out(cipher)
        ); 
endmodule
