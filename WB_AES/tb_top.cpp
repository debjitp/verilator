#include "verilated.h"
#include "Vwb_top.h"
#include "AES.h"


///////////////////////////




//#define VM_TRACE 1
#if VM_TRACE 
#include "verilated_vcd_c.h"
//VCD file gen
#endif 



Vwb_top* top;
#if VM_TRACE 
VerilatedVcdC* tfp = new VerilatedVcdC; 
//VCD file gen
#endif




void evalModel() {
    top->eval();
#if VM_TRACE
    tfp->dump(main_time);//VCD file gen
    // tfp -> dump(cipher);
#endif
}

void toggleClock() {
    top->clk = ~top->clk;
}

void toggleClock0() {
    top->clk = 0;
}

void toggleClock1() {
    top->clk = 1;
}


void waitForACK() {
    while(top->busms_ack_i == 0) {
        runForClockCycles(1);
    }
}


void runForClockCycles(const unsigned int pCycles) {
    int doubleCycles = pCycles << 2;
    while(doubleCycles > 0) {
        if (main_time > 10 ) {
            top->rst = 0;
        }
        if((main_time % 10) == 1) {
            //toggleClock1();
            top->clk = 0;
        }
        if ((main_time %10) == 6) {
            //toggleClock0();
            top->clk = 1;
        }
        evalModel();
        //toggleClock();
        //main_time += (CLOCK_PERIOD >> 2);
        main_time++;
        --doubleCycles;
        printf("doubleCycles: %d, clk: %d.\n", doubleCycles, top->clk);
    }
}





uint32_t readFromAddress(uint32_t pAddress) {
    top->busms_adr_o = pAddress ;
    top->busms_dat_o = 0x00000000;
    top->busms_we_o = 0;
    top->busms_stb_o = 1;
    runForClockCycles(20);
    //waitForACK();
    uint32_t data = top->busms_dat_i;
    top->busms_stb_o = 0;
    
    return data;
}

void writeToAddress(uint32_t pAddress, uint32_t pData) {
    top->busms_adr_o = pAddress;
    top->busms_dat_o = pData;
    top->busms_we_o = 1;
    top->busms_stb_o = 1;
    runForClockCycles(10);
    //waitForACK();
    top->busms_stb_o = 0;
    top->busms_we_o = 0;
    runForClockCycles(10);
}


bool ciphertextValid(void) {
	uint32_t a = readFromAddress(AES_DONE);
	uint32_t b = AES_DONE;
	//printf("aes_add %d" , b);
	//printf ("read %d\n", a);
    return (readFromAddress(AES_DONE) != 0) ? true : false;

}

void start(void) {
    writeToAddress(AES_START, 0x1);
    writeToAddress(AES_START, 0x0);
}

void saveCiphertext(uint32_t *pCT) {
    for(unsigned int i = 0; i < BLOCK_WORDS; ++i) {
        pCT[(BLOCK_WORDS - 1) - i] = readFromAddress(AES_CT_BASE + (i * BYTES_PER_WORD));
    }
}

void reportCiphertext(void) {
    printf("Ciphertext:\t0x");
    uint32_t ct[BLOCK_WORDS];
    
    saveCiphertext(ct);
    for(unsigned int i = 0; i < BLOCK_WORDS; ++i) {
      printf("%08X", ct[(BLOCK_WORDS - 1) - i]);
    }
    printf("\n");
}

void setPlaintext(const char* pPT) {
    printf("Plaintext:\t0x");
    for(unsigned int i = 0; i < BLOCK_BYTES; ++i) {
        uint32_t temp;
        for(int j = 0; j < 4; ++j) {
            ((char *)(&temp))[(BLOCK_WORDS - 1) - j] = *pPT++;
        }
        writeToAddress(AES_PT_BASE + (((BLOCK_WORDS - 1) - i) * BYTES_PER_WORD), temp);
        printf("%08X", temp);
    }
    printf("\n");
}

void setPlaintext(const uint32_t* pPT) {
    printf("Plaintext:\t0x");
    for(unsigned int i = 0; i < BLOCK_WORDS; ++i) {
        writeToAddress(AES_PT_BASE + (((BLOCK_WORDS - 1) - i) * BYTES_PER_WORD), pPT[i]);
        printf("%08X", pPT[i]);
    }
    printf("\n");
}

void setKey(const uint32_t* pKey) {
    printf("Key:\t\t0x");
    for(unsigned int i = 0; i < KEY_WORDS; ++i) {
        writeToAddress(AES_KEY_BASE + (((KEY_WORDS - 1) - i) * BYTES_PER_WORD), pKey[i]);
        printf("%08X", pKey[i]);
    }
    printf("\n");
}

int main(int argc, char **argv, char **env) {
    Verilated::commandArgs(argc, argv);
#if VM_TRACE
    Verilated::traceEverOn(true); 
//VCD file gen
#endif

    top = new Vwb_top;
    
	uint32_t ct[BLOCK_WORDS];

#if VM_TRACE
  printf("Initializing traces and opening VCD file\n");
  top->trace (tfp, 99);
//VCD file gen 
  tfp->open("./obj_dir/Vwb_top.vcd");
//VCD file gen
#endif
    
    printf("Initializing interface and resetting core\n");
    
    // Initialize Inputs
    top->clk = 0;
    top->rst = 1;
    top->busms_dat_o = 0;
    top->busms_we_o = 0;
    top->busms_adr_o = 0;
    top->busms_stb_o = 0;
    
    printf("Reset complete\n");
    
uint32_t pt1[4] ={ 0x3243f6a8, 0x885a308d, 0x313198a2, 0xe0370734};
uint32_t key1[6] ={ 0x2b7e1516, 0x28aed2a6, 0xabf71588, 0x09cf4f3c, 0x2b7e1516, 0x28aed2a6};
    setPlaintext(pt1);
    setKey(key1);
    start();
    waitForValidOutput();
    reportCiphertext();
    verifyCiphertext("4fcb8db85784a2c1bb77db7ede3217ac", "test 1");    


#if VM_TRACE    
	printf ( "TASHFIA\n") ;
    tfp->close(); 
//VCD file gen
#endif
    top->final();
    delete top;
    exit(0);
}













